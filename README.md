# Vortrag_Passwortsicherheit

Vortrag zum Thema Passwortsicherheit (erstellt mit LaTeX)

Dieser Vortrag, ausgearbeitet für die Nacht des Wissens am 20.10.2018, soll vermitteln, wie wichtig es ist, sich mit dem Umgang mit Passwörtern zu beschäftigen.
Es wird aufgezeigt, wie weit Passwörter in unserem täglichen Leben vorgedrungen sind und was passieren kann, wenn diese von anderen missbraucht werden.
Weiterhin werden Vorschläge unterbreitet, wie man Passwörter und den Umgang mit ihnen sicherer gestalten kann.

Es lohnt sich, die ausführlichen Quellenangaben - eine gut recherchierte Linksammlung zum Thema - durzustöbern, um sich interessant und humorvoll mit dem Thema Passwortsicherheit auch nach dem Vortrag zu beschäftigen.